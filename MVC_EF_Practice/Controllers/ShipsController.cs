﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MVC_EF_Practice.Helpers;
using MVC_EF_Practice.Models;

namespace MVC_EF_Practice.Controllers
{
  public class ShipsController : Controller
  {

    // GET: Ships
    public async Task<IActionResult> Index()
    {
      var responseData = await HttpClientHelpers.GetHttpGetData<IEnumerable<Ship>>("api/ships");

      return View(responseData);
    }

    // GET: Ships/Details/5
    public async Task<IActionResult> Details(int? id)
    {
      if (id == null)
      {
        return NotFound();
      }

      var ship = await HttpClientHelpers.GetHttpGetData<Ship>($"api/ships/{id}");
      if (ship == null)
      {
        return NotFound();
      }

      return View(ship);
    }

    // GET: Ships/Create
    public IActionResult Create()
    {
      return View();
    }

    // POST: Ships/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("Id,Name,FactionId")] Ship ship)
    {
      if (ModelState.IsValid)
      {
        if (await HttpClientHelpers.GetHttpPostResult("/api/ships", ship))
        {
          return RedirectToAction(nameof(Index));
        }
      }

      return View(ship);
    }

    // GET: Ships/Edit/5
    public async Task<IActionResult> Edit(int? id)
    {
      if (id == null)
      {
        return NotFound();
      }

      var ship = await HttpClientHelpers.GetHttpGetData<Ship>($"api/ships/{id}");
      if (ship == null)
      {
        return NotFound();
      }
      return View(ship);
    }

    // POST: Ships/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(int id, [Bind("Id,Name,FactionId")] Ship ship)
    {
      if (id != ship.Id)
      {
        return NotFound();
      }

      if (ModelState.IsValid)
      {
        if (await HttpClientHelpers.GetHttpPutResult($"api/ships/{id}", ship))
        {
          return RedirectToAction(nameof(Index));
        }
      }
      return View(ship);
    }

    // GET: Ships/Delete/5
    public async Task<IActionResult> Delete(int? id)
    {
      if (id == null)
      {
        return NotFound();
      }

      var ship = await HttpClientHelpers.GetHttpGetData<Ship>($"api/ships/{id}");
      if (ship == null)
      {
        return NotFound();
      }

      return View(ship);
    }

    // POST: Ships/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(int id)
    {
      await HttpClientHelpers.GetHttpDeleteResponse($"api/ships/{id}");

      return RedirectToAction(nameof(Index));
    }

  }
}
