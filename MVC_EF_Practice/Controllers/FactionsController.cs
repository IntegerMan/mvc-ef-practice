﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MVC_EF_Practice.Helpers;
using MVC_EF_Practice.Models;

namespace MVC_EF_Practice.Controllers
{
  public class FactionsController : Controller
  {

    // GET: Factions
    public async Task<IActionResult> Index()
    {
      var responseData = await HttpClientHelpers.GetHttpGetData<IEnumerable<Faction>>("api/factions");

      return View(responseData);
    }

    // GET: Factions/Details/5
    public async Task<IActionResult> Details(int? id)
    {
      if (id == null)
      {
        return NotFound();
      }

      var faction = await HttpClientHelpers.GetHttpGetData<Faction>($"api/factions/{id}");
      if (faction == null)
      {
        return NotFound();
      }

      return View(faction);
    }

    // GET: Factions/Create
    public IActionResult Create()
    {
      return View();
    }

    // POST: Factions/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("Id,Name")] Faction faction)
    {
      if (ModelState.IsValid)
      {
        if (await HttpClientHelpers.GetHttpPostResult("/api/factions", faction))
        {
          return RedirectToAction(nameof(Index));
        }
      }
      return View(faction);
    }

    // GET: Factions/Edit/5
    public async Task<IActionResult> Edit(int? id)
    {
      if (id == null)
      {
        return NotFound();
      }

      var faction = await HttpClientHelpers.GetHttpGetData<Faction>($"api/factions/{id}");
      if (faction == null)
      {
        return NotFound();
      }
      return View(faction);
    }

    // POST: Factions/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(int id, [Bind("Id,Name")] Faction faction)
    {
      if (id != faction.Id)
      {
        return NotFound();
      }

      if (ModelState.IsValid)
      {
        if (await HttpClientHelpers.GetHttpPutResult($"api/factions/{id}", faction))
        {
          return RedirectToAction(nameof(Index));
        }
        
      }
      return View(faction);
    }

    // GET: Factions/Delete/5
    public async Task<IActionResult> Delete(int? id)
    {
      if (id == null)
      {
        return NotFound();
      }

      var faction = await HttpClientHelpers.GetHttpGetData<Faction>($"api/factions/{id}");
      if (faction == null)
      {
        return NotFound();
      }

      return View(faction);
    }

    // POST: Factions/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(int id)
    {
      await HttpClientHelpers.GetHttpDeleteResponse($"api/factions/{id}");

      return RedirectToAction(nameof(Index));
    }

  }
}
