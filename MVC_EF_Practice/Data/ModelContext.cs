﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MVC_EF_Practice.Models;

namespace MVC_EF_Practice.Models
{
    public class ModelContext : DbContext
    {
        public ModelContext (DbContextOptions<ModelContext> options)
            : base(options)
        {
        }

        public DbSet<MVC_EF_Practice.Models.Ship> Ship { get; set; }

        public DbSet<MVC_EF_Practice.Models.Faction> Faction { get; set; }
    }
}
