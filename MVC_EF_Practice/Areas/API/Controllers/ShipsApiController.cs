﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MVC_EF_Practice.Models;
using MVC_EF_Practice.Services;

namespace MVC_EF_Practice.Controllers
{
  [Produces("application/json")]
  [Route("api/ships")]
  public class ShipsApiController : Controller
  {
    private readonly ShipRepository _shipRepository;

    public ShipsApiController(ShipRepository shipRepository)
    {
      _shipRepository = shipRepository;
    }

    // GET: api/ships
    [HttpGet]
    public async Task<IEnumerable<Ship>> GetShips()
    {
      return await _shipRepository.GetShipsAsync();
    }

    // GET: api/ships/5
    [HttpGet("{id}")]
    public IActionResult GetShip([FromRoute] int id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      var ship = _shipRepository.GetShip(id);
      if (ship == null)
      {
        return NotFound();
      }

      return Ok(ship);
    }

    // PUT: api/ships/5
    [HttpPut("{id}")]
    public async Task<IActionResult> PutShip([FromRoute] int id, [FromBody] Ship ship)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      if (id != ship.Id)
      {
        return BadRequest();
      }


      try
      {
        await _shipRepository.UpdateShipAsync(ship);
      }
      catch (DbUpdateConcurrencyException)
      {
        var dbShip = _shipRepository.GetShip(id);
        if (dbShip == null)
        {
          return NotFound();
        }
        else
        {
          throw;
        }
      }

      return NoContent();
    }

    // POST: api/ships
    [HttpPost]
    public async Task<IActionResult> PostShip([FromBody] Ship ship)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      await _shipRepository.AddAsync(ship);

      return CreatedAtAction("GetShips", new { id = ship.Id }, ship);
    }

    // DELETE: api/ships/5
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteShip([FromRoute] int id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      var ship = _shipRepository.GetShip(id);
      if (ship == null)
      {
        return NotFound();
      }

      await _shipRepository.DeleteShipAsync(ship);

      return Ok(ship);
    }

  }
}