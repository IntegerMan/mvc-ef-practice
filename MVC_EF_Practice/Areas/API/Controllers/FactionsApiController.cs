﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MVC_EF_Practice.Models;
using MVC_EF_Practice.Services;

namespace MVC_EF_Practice.Controllers
{
  [Produces("application/json")]
  [Route("api/factions")]
  public class FactionsApiController : Controller
  {
    private readonly FactionRepository _factionRepository;

    public FactionsApiController(FactionRepository factionRepository)
    {
      _factionRepository = factionRepository;
    }

    // GET: api/factions
    [HttpGet]
    public async Task<IEnumerable<Faction>> GetFaction()
    {
      return await _factionRepository.GetFactionsAsync();
    }

    // GET: api/factions/5
    [HttpGet("{id}")]
    public async Task<IActionResult> GetFaction([FromRoute] int id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      var faction = await _factionRepository.GetFactionAsync(id);
      if (faction == null)
      {
        return NotFound();
      }

      return Ok(faction);
    }

    // PUT: api/factions/5
    [HttpPut("{id}")]
    public async Task<IActionResult> PutFaction([FromRoute] int id, [FromBody] Faction faction)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      if (id != faction.Id)
      {
        return BadRequest();
      }


      try
      {
        await _factionRepository.UpdateFactionAsync(faction);
      }
      catch (DbUpdateConcurrencyException)
      {
        var dbFaction = await _factionRepository.GetFactionAsync(id);

        if (dbFaction == null)
        {
          return NotFound();
        }
        else
        {
          throw;
        }
      }

      return NoContent();
    }

    // POST: api/factions
    [HttpPost]
    public async Task<IActionResult> PostFaction([FromBody] Faction faction)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      await _factionRepository.AddAsync(faction);

      return CreatedAtAction("GetFaction", new { id = faction.Id }, faction);
    }

    // DELETE: api/factions/5
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteFaction([FromRoute] int id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      var faction = await _factionRepository.GetFactionAsync(id);
      if (faction == null)
      {
        return NotFound();
      }

      await _factionRepository.DeleteFactionAsync(faction);

      return Ok(faction);
    }

  }
}