﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MVC_EF_Practice.Models;

namespace MVC_EF_Practice.Services
{
  public class FactionRepository
  {
    private readonly ModelContext _context;

    public FactionRepository(ModelContext context)
    {
      _context = context;
    }

    public IEnumerable<Faction> GetFactions()
    {
      return _context.Faction.ToImmutableList();
    }

    public Task<List<Faction>> GetFactionsAsync()
    {
      return _context.Faction.ToListAsync();
    }

    internal Task<Faction> GetFactionAsync(int? id)
    {
      return _context.Faction.SingleOrDefaultAsync(m => m.Id == id);
    }

    internal Task<int> AddAsync(Faction faction)
    {
      _context.Add(faction);

      return _context.SaveChangesAsync();
    }

    internal Task<int> UpdateFactionAsync(Faction faction)
    {
      _context.Update(faction);

      return _context.SaveChangesAsync();
    }

    internal Task DeleteFactionAsync(Faction faction)
    {
      _context.Faction.Remove(faction);

      return _context.SaveChangesAsync();
    }


  }
}