﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MVC_EF_Practice.Models;

namespace MVC_EF_Practice.Services
{
  public class ShipRepository
  {
    private readonly ModelContext _context;

    public ShipRepository(ModelContext context)
    {
      _context = context;
    }

    internal Task<List<Ship>> GetShipsAsync()
    {
      return _context.Ship.ToListAsync();
    }

    internal Ship GetShip(int id)
    {
      var factionShips = _context.Ship.Include(s => s.Faction);
      var match = factionShips.FirstOrDefault(m => m.Id == id);

      return match;
    }

    internal Task<Ship> GetShipAsync(int id)
    {
      var factionShips = _context.Ship.Include(s => s.Faction);
      var match = factionShips.FirstOrDefaultAsync(m => m.Id == id);

      return match;
    }

    internal Task<int> AddAsync(Ship ship)
    {
      _context.Add(ship);

      return _context.SaveChangesAsync();
    }

    internal Task<int> UpdateShipAsync(Ship ship)
    {
      _context.Update(ship);

      return _context.SaveChangesAsync();
    }

    internal Task DeleteShipAsync(Ship ship)
    {
      _context.Ship.Remove(ship);

      return _context.SaveChangesAsync();
    }
  }
}