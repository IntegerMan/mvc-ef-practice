﻿using System.ComponentModel.DataAnnotations;

namespace MVC_EF_Practice.Models
{
  public class Ship
  {

    public int Id { get; set; }

    [Required]
    [MaxLength(140)]
    public string Name { get; set; }

    [Display(Name = "Faction")]
    public int FactionId { get; set; }
    public Faction Faction { get; set; }
  }
}