﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace MVC_EF_Practice.Models
{
    public class Faction
    {
      public int Id { get; set; }
      
      [Required]
      [MaxLength(140)]
      public string Name { get; set; }

      [IgnoreDataMember]
      public IEnumerable<Ship> Ships { get; set; }
    }
}
