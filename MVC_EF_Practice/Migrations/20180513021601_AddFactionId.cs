﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace MVC_EF_Practice.Migrations
{
    public partial class AddFactionId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Ship_Faction_FactionId",
                table: "Ship");

            migrationBuilder.AlterColumn<int>(
                name: "FactionId",
                table: "Ship",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Ship_Faction_FactionId",
                table: "Ship",
                column: "FactionId",
                principalTable: "Faction",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Ship_Faction_FactionId",
                table: "Ship");

            migrationBuilder.AlterColumn<int>(
                name: "FactionId",
                table: "Ship",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Ship_Faction_FactionId",
                table: "Ship",
                column: "FactionId",
                principalTable: "Faction",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
